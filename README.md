# quasar-css

## References

- https://quasar.dev/style/typography
- https://quasar.dev/style/sass-scss-variables
- https://quasar.dev/start/vite-plugin + https://www.npmjs.com/package/quasar

## Development

VS Code profile: [Rome](https://github.com/joaopalmeiro/vscode-profiles/tree/main/Rome)

```bash
nvm install && nvm use && node --version
```

or

```bash
nvm use && node --version
```

```bash
npm install
```

```bash
npm run dev
```

or

```bash
npm run dev:sass
```

## Notes

- `npm install css-tree && npm install -D typescript ts-node @tsconfig/node18 @types/css-tree quasar@2.11.4`
- `npm install -D rome` + `npx rome init`
- https://github.com/microsoft/vscode/issues/138048
- https://docs.rome.tools/lint/rules/ + https://docs.rome.tools/linter/#enable-a-lint-rule
- `npm install sass autoprefixer@10.4.13 postcss && npm install -D type-fest`
- https://jvilk.com/MakeTypes/
