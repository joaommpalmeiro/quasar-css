import {
  CSS_PATH,
  FONT_WEIGHT_CLASSES,
  HEADING_CLASSES,
  MISC_TYPOGRAPHY_CLASSES,
} from './constants';
import type { Section, Token } from './types';
import { generateProperties } from './utils';
import { parse, walk } from 'css-tree';
import { readFileSync, writeFileSync } from 'node:fs';

const quasarCSS = readFileSync(CSS_PATH, { encoding: 'utf8' });
// console.log(quasarCSS);

const ast = parse(quasarCSS);
// console.log(ast);
// console.log(JSON.stringify(ast, null, 2));
// https://github.com/csstree/csstree/blob/master/docs/utils.md#toplainobjectast
// import { clone, toPlainObject } from 'css-tree';
// writeFileSync('ast.json', JSON.stringify(toPlainObject(clone(ast)), null, 2));

// https://github.com/csstree/csstree/blob/v2.3.1/docs/ast.md
// https://github.com/csstree/csstree/blob/v2.3.1/docs/traversal.md
// https://quasar.dev/style/typography
// https://github.com/quasarframework/quasar/blob/quasar-v2.11.4/ui/src/css/core/typography.sass
// https://astexplorer.net/
// https://tailwindcss.com/docs/font-size
// https://github.com/csstree/csstree/issues/78
// https://bobbyhadz.com/blog/javascript-replace-first-occurrence-of-character-in-string

const headings: Token[] = [];
const fontWeights: Token[] = [];
const miscTypography: Token[] = [];

walk(ast, {
  visit: 'Rule',
  enter(ruleNode) {
    walk(ruleNode, {
      visit: 'ClassSelector',
      enter(classSelectorNode) {
        const className = classSelectorNode.name;

        if (HEADING_CLASSES.includes(className)) {
          const props = generateProperties(ruleNode);
          const token: Token = {
            class: className,
            properties: props,
          };
          headings.push(token);
        } else if (FONT_WEIGHT_CLASSES.includes(className)) {
          const props = generateProperties(ruleNode);
          const token: Token = {
            class: className,
            properties: props,
          };
          fontWeights.push(token);
        } else if (MISC_TYPOGRAPHY_CLASSES.includes(className)) {
          const props = generateProperties(ruleNode);
          const token: Token = {
            class: className,
            properties: props,
          };
          miscTypography.push(token);
        }
        // else {
        //   console.log(`Class ${className} ignored`);
        // }
      },
    });
  },
});

// console.log(headings, JSON.stringify(headings));

const output: Section[] = [
  {
    // https://quasar.dev/style/typography#introduction
    name: 'Typography',
    subsections: [
      {
        name: 'Headings',
        classes: headings,
      },
      {
        name: 'Font Weights',
        classes: fontWeights,
      },
      {
        name: 'Misc',
        classes: miscTypography,
      },
    ],
  },
];

// https://github.com/rome/tools/blob/cli/v11.0.0/website/src/pages/lint/rules/noConditionalAssignment.md

// Issue (Rome 11): https://github.com/rome/tools/issues/4086
// (noPrecisionLoss reports 1 digit integers)
// https://docs.rome.tools/lint/rules/noprecisionloss/
const outputText = JSON.stringify(output, null, 2);
console.log(outputText);

writeFileSync('quasar.json', outputText);

console.log('All done!');
