import type { Rule } from 'css-tree';
import { findAll, generate } from 'css-tree';

export const generateProperties = (ruleNode: Rule): string => {
  const decls = findAll(ruleNode, (node) => node.type === 'Declaration');
  return decls
    .map((decl) => `${generate(decl).replace(':', ': ')};`)
    .join('\n');
};
