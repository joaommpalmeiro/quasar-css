export const CSS_PATH = './node_modules/quasar/dist/quasar.css';
export const TYPOGRAPHY_SASS_PATH =
  './node_modules/quasar/src/css/core/typography.sass';
export const VARIABLES_SASS_PATH =
  './node_modules/quasar/src/css/variables.sass';
export const PACKAGE_JSON_PATH = './node_modules/quasar/package.json';

export const HEADING_CLASSES: string[] = [
  'text-h1',
  'text-h2',
  'text-h3',
  'text-h4',
  'text-h5',
  'text-h6',
  'text-subtitle1',
  'text-subtitle2',
  'text-body1',
  'text-body2',
  'text-caption',
  'text-overline',
];

export const FONT_WEIGHT_CLASSES: string[] = [
  'text-weight-thin',
  'text-weight-light',
  'text-weight-regular',
  'text-weight-medium',
  'text-weight-bold',
  'text-weight-bolder',
];

export const MISC_TYPOGRAPHY_CLASSES: string[] = [
  'text-right',
  'text-left',
  'text-center',
  'text-justify',
  'text-bold',
  'text-italic',
  'text-no-wrap',
  'text-strike',
  'text-uppercase',
  'text-lowercase',
  'text-capitalize',
];
