import type { PackageJson } from 'type-fest';

export type Token = {
  readonly class: string;
  readonly properties: string;
};

export type Subsection = {
  readonly name: string;
  readonly classes: Token[];
};

export type Section = {
  readonly name: string;
  readonly subsections: Subsection[];
};

// https://github.com/browserslist/browserslist
// https://github.com/quasarframework/quasar/blob/quasar-v2.11.4/ui/package.json
export type QuasarPackageJson = PackageJson & {
  browserslist: string[];
};
