// https://github.com/sass/dart-sass/issues/1775
// https://sass-lang.com/documentation/js-api/
// https://sass-lang.com/documentation/js-api/modules#compileString
// https://www.npmjs.com/package/sass
// https://github.com/quasarframework/quasar/blob/quasar-v2.11.4/ui/build/script.build.css.js
// https://github.com/quasarframework/quasar/blob/quasar-v2.11.4/ui/package.json#L75
// https://github.com/quasarframework/quasar/blob/quasar-v2.11.4/ui/package.json#L108
// https://autoprefixer.github.io/
// https://github.com/postcss/autoprefixer/blob/10.4.13/pnpm-lock.yaml#L25
// https://github.com/browserslist/caniuse-lite/tree/1.0.30001426
// https://www.npmjs.com/package/caniuse-lite/v/1.0.30001426?activeTab=explore
// https://github.com/postcss/autoprefixer/blob/10.4.13/data/prefixes.js#L495
// https://github.com/quasarframework/quasar/blob/quasar-v2.11.4/ui/package.json#L78
// https://github.com/cssnano/cssnano/blob/cssnano%405.1.14/pnpm-lock.yaml#L54

import {
  PACKAGE_JSON_PATH,
  TYPOGRAPHY_SASS_PATH,
  VARIABLES_SASS_PATH,
} from './constants';
import type { QuasarPackageJson, Subsection, Token } from './types';
import { generateProperties } from './utils';
import autoprefixer from 'autoprefixer';
import { parse, walk } from 'css-tree';
import { writeFileSync } from 'node:fs';
import { readFile } from 'node:fs/promises';
import postcss from 'postcss';
import { compileString } from 'sass';

const main = async () => {
  try {
    const [typographySass, varSass, pkgJson] = await Promise.all([
      readFile(TYPOGRAPHY_SASS_PATH, { encoding: 'utf8' }),
      readFile(VARIABLES_SASS_PATH, { encoding: 'utf8' }),
      readFile(PACKAGE_JSON_PATH, { encoding: 'utf8' }),
    ]);
    // console.log({ typographySass, varSass, pkgJson });

    const processedPkgJson: QuasarPackageJson = JSON.parse(pkgJson);
    // console.log(processedPkgJson);

    const { css } = compileString(`${varSass}\n${typographySass}`, {
      syntax: 'indented',
    });
    // console.log(css);

    // https://github.com/postcss/autoprefixer/tree/10.4.13#javascript
    // https://evilmartians.com/chronicles/postcss-8-plugin-migration
    // https://github.com/postcss/autoprefixer/tree/10.4.13#options
    // https://github.com/sindresorhus/type-fest/blob/main/source/package-json.d.ts
    // https://github.com/DefinitelyTyped/DefinitelyTyped/issues/27814
    const { css: processedCss } = await postcss([
      autoprefixer({ overrideBrowserslist: processedPkgJson.browserslist }),
    ]).process(css, { from: undefined });
    // console.log(processedCss);

    // const ast = parse(css);
    const ast = parse(processedCss);
    // console.log(JSON.stringify(ast, null, 2));

    const typography: Token[] = [];
    walk(ast, {
      visit: 'ClassSelector',
      enter(node) {
        // https://github.com/csstree/csstree/blob/master/docs/traversal.md#enter
        const className = node.name;
        if (this.rule) {
          // console.log(this);
          // console.log(this.block);
          // console.log(this.rule);
          const props = generateProperties(this.rule);
          const token: Token = {
            class: className,
            properties: props,
          };
          typography.push(token);
        }
      },
    });

    const output: Subsection[] = [
      {
        name: 'Typography',
        classes: typography,
      },
    ];

    const outputText = JSON.stringify(output, null, 2);
    // console.log(outputText);

    writeFileSync('quasar.json', outputText);

    console.log('All done!');
  } catch (e) {
    console.error(e);
  }
};

main();
